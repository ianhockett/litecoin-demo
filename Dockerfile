# The following references were used:
#   - https://wiki.alpinelinux.org
#   - https://pkgs.alpinelinux.org/packages
#   - https://bitcoin.stackexchange.com/questions/65156/how-do-i-verify-the-gpg-sig-for-litecoin-core
#   - https://stackoverflow.com/questions/37725969/several-pgp-signatures-for-one-file
#   - https://docs.docker.com/engine/reference/builder/
#   - https://github.com/uphold/docker-litecoin-core # used mainly for inspiration; opted to create a minimally functional solution by hand rather than copy/pasta

FROM debian:stable-slim

RUN DEBIAN_FRONTEND=noninteractive && \
    apt-get autoclean -y && \
    apt-get -y update && \
    apt-get -y upgrade && \
    apt-get -y -q install gnupg wget && \
    wget "https://download.litecoin.org/litecoin-0.18.1/linux/litecoin-0.18.1-linux-signatures.asc" && \
    wget "https://download.litecoin.org/litecoin-0.18.1/linux/litecoin-0.18.1-x86_64-linux-gnu.tar.gz" && \
    gpg --keyserver keyserver.ubuntu.com --recv-key FE3348877809386C && \
    gpg --decrypt litecoin-0.18.1-linux-signatures.asc 1>litecoin-sigs && \
    export sig="$(grep '0.18.1-x86_64' litecoin-sigs)" && \
    export pkg="$(sha256sum litecoin-0.18.1-x86_64-linux-gnu.tar.gz)" && \
    if [ "$sig" != "$pkg" ]; then exit 1; fi && \
    tar -xf litecoin-0.18.1-x86_64-linux-gnu.tar.gz && \
    useradd -r litecoin && \
    mkdir -p /data/litecoin.d && \
    chmod 770 /data/litecoin.d && \
    chown -R litecoin /data/litecoin.d

USER litecoin

CMD ["litecoin-0.18.1/bin/litecoind","-datadir=/data/litecoin.d"]
