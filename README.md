# Litecoin Docker Demo

This is a demo project with a docker image for Litecoin Core, a Kubernetes stateful set configuration, and a Gitlab CI file to tie it all together.

>Note: Dockerfile, litecoin-statefulset.yml, and the _build_ stage of .gitlab-ci.yml have all been tested and confirmed working. However, the Kubernetes piece was only tested using a local cluster. Therefore, the _deploy_ stage of .gitlab-ci.yml is still untested.

## References

The following references were used:

- https://wiki.alpinelinux.org
- https://pkgs.alpinelinux.org/packages
- https://bitcoin.stackexchange.com/questions/65156/how-do-i-verify-the-gpg-sig-for-litecoin-core
- https://stackoverflow.com/questions/37725969/several-pgp-signatures-for-one-file
- https://docs.docker.com/engine/reference/builder/
- https://github.com/uphold/docker-litecoin-core
- https://sanderknape.com/2019/02/automated-deployments-kubernetes-gitlab/
- https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/
- https://kubernetes.io/docs/reference/kubectl/cheatsheet/
- https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands